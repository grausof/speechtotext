package cordova-plugin-speech-to-text;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class SpeechToText extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("startListening")) {
            String message = args.getString(0);
            this.startListening(message, callbackContext);
            return true;
        }
        if (action.equals("hasPermission")) {
            String message = args.getString(0);
            this.hasPermission(message, callbackContext);
            return true;
        }
        if (action.equals("requestPermission")) {
            String message = args.getString(0);
            this.requestPermission(message, callbackContext);
            return true;
        }
        return false;
    }

    private void startListening(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void requestPermission(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void hasPermission(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
