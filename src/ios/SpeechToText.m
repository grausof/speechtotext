/********* SpeechToText.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <Speech/Speech.h>
@interface SpeechToText : CDVPlugin {
  // Member variables go here.
}
- (void)hasPermission:(CDVInvokedUrlCommand*)command;
- (void)startListening:(CDVInvokedUrlCommand*)command;
- (void)requestPermission:(CDVInvokedUrlCommand*)command;
- (void)isRecognitionAvailable:(CDVInvokedUrlCommand*)command;

@end

@implementation SpeechToText

- (void)startListening:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSDictionary* requestCommand = [command.arguments objectAtIndex:0];
    NSLog(@"%@",requestCommand);

    if (requestCommand != nil) {
        
        NSString*language = [requestCommand valueForKey:@"language"];
        NSString*filepath = [requestCommand valueForKey:@"path"];
        NSLog(@"Language: %@", language);
        NSLog(@"File path: %@", filepath);
        @try {
            NSLocale *local =[[NSLocale alloc] initWithLocaleIdentifier:language];
            SFSpeechRecognizer*speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:local];
            
            NSString *soundFilePath = filepath;
            
            
            NSURL *url = [[NSURL alloc] initFileURLWithPath:soundFilePath];
            if(!speechRecognizer.isAvailable){
                    NSLog(@"speechRecognizer is not available, maybe it has no internet connection");
                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }
            
            SFSpeechURLRecognitionRequest *urlRequest = [[SFSpeechURLRecognitionRequest alloc] initWithURL:url];
            urlRequest.shouldReportPartialResults = YES; // YES if animate writting
            
                
            [speechRecognizer recognitionTaskWithRequest: urlRequest resultHandler:  ^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error)
             {
                 
                 NSString *transcriptions = result.bestTranscription.formattedString;
                 
                 //NSLog(@"startListening() recognitionTask result array: %@", transcriptions.description);
                 
                 CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:transcriptions];

                     [pluginResult setKeepCallbackAsBool:YES];
                 
                     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                 
             }];
            
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.reason);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
         [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }

   
}

- (void)hasPermission:(CDVInvokedUrlCommand*)command {
    SFSpeechRecognizerAuthorizationStatus status = [SFSpeechRecognizer authorizationStatus];
    BOOL speechAuthGranted = (status == SFSpeechRecognizerAuthorizationStatusAuthorized);
    
    if (!speechAuthGranted) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:NO];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    } else {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    
    
}

- (void)isRecognitionAvailable:(CDVInvokedUrlCommand*)command {
    CDVPluginResult *pluginResult = nil;
    
    if ([SFSpeechRecognizer class]) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:NO];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)requestPermission:(CDVInvokedUrlCommand*)command
{
  
    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
        NSString*result = @"";
        switch (status) {
            case SFSpeechRecognizerAuthorizationStatusAuthorized:
                NSLog(@"Authorized");
                result = @"Authorized";
                break;
            case SFSpeechRecognizerAuthorizationStatusDenied:
                NSLog(@"Denied");
                result = @"Denied";
                break;
            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
                NSLog(@"Not Determined");
                result = @"Not Determined";
                break;
            case SFSpeechRecognizerAuthorizationStatusRestricted:
                NSLog(@"Restricted");
                result = @"Restricted";
                break;
            default:
                break;
        }
        [self sendResult:result andCommand:command];
    }];
    
}

-(void)sendResult:(NSString*)result andCommand:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
