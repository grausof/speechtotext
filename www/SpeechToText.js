var exec = require('cordova/exec');

exports.hasPermission = function ( success, error) {
    exec(success, error, 'SpeechToText', 'hasPermission', []);
};

exports.startListening = function (arg0, success, error) {
    exec(success, error, 'SpeechToText', 'startListening', [arg0]);
};

exports.requestPermission = function (success, error) {
    exec(success, error, 'SpeechToText', 'requestPermission', []);
};

exports.isRecognitionAvailable = function (success, error) {
    exec(success, error, 'SpeechToText', 'isRecognitionAvailable', []);
};
